# Docker instructions
Navigate to the 'docker' folder that contains the Dockerfile and run the following command to build the image:

```docker build -t palisade-development .```

Once the image is built, run the container by using:

```docker run -dit --name palisade -p 80:80 palisade-development```

To get a shell into the container run:

```docker exec -it palisade /bin/bash``` 

Type 'exit' to leave the container shell.

To clean all containers once palisade-development is done running

```docker kill palisade```

```docker container prune```
